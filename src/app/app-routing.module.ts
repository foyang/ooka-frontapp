import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EquipmentComponent} from "./equipment/equipment.component";


const routes: Routes = [
  { path: '', component: EquipmentComponent, pathMatch: 'full' },
  { path: '', redirectTo: 'equipment', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
