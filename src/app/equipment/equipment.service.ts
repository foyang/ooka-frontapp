import { Injectable } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {Equipment} from "./equipment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {OilSystem} from "./oil-system";
import {FuelSystem} from "./fuel-system";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class EquipmentService {
  SERVER_BASE_URL_EquipmentService = "http://localhost:9001/";
  SERVER_BASE_URL_PersistService = "http://localhost:9002/";
  constructor(private http: HttpClient) {
  }

  public form: FormGroup = new FormGroup({
    id: new FormControl(),
    oilSystems: new FormControl(),
    fuelSystem: new FormControl(),
    gearboxOptions: new FormControl()
  });

  addEquipment(equipment: Equipment) {
    return this.http.post<any>(this.SERVER_BASE_URL_EquipmentService + 'service-equipment/add', equipment,httpOptions);
  }

  getOptionEquipments() {
    return this.http.get<any>(this.SERVER_BASE_URL_EquipmentService + 'option-equipment/all',httpOptions);
  }

  checkOilValue(oilSystem: OilSystem){
    return this.http.post<any>(this.SERVER_BASE_URL_EquipmentService + 'service-equipment/valid/oil', oilSystem ,httpOptions);
  }

  checkFuelValue(fuel: FuelSystem){
    return this.http.post<any>(this.SERVER_BASE_URL_EquipmentService + 'service-equipment/valid/fuel', fuel ,httpOptions);
  }

  getAllPersistEquipment() {
    return this.http.get<any>(this.SERVER_BASE_URL_PersistService + 'equipment/all',httpOptions);
  }
}

