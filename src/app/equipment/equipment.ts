export class Equipment {
  id: number;
  oilSystem: string;
  fuelSystem: string;
  gearboxOptions: string;
}
