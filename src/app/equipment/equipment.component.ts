import { Component, OnInit } from '@angular/core';
import {EquipmentService} from "./equipment.service";
import {Equipment} from "./equipment";
import {OilSystem} from "./oil-system";
import {FuelSystem} from "./fuel-system";
import {NotificationFlashService} from "../notification-service/notification-flash.service";


@Component({
  selector: 'app-equipment',
  templateUrl: './equipment.component.html',
  styleUrls: ['./equipment.component.css']
})
export class EquipmentComponent implements OnInit {
  displayedColumns: string[] = ['position', 'oil', 'fuel', 'gearbox'];
  equipment = new Equipment;
  oilValue = new OilSystem();
  fuelValue = new FuelSystem();
  optionEquipments: any;
  oilSystems: string[] = [];
  fuelSystems: string[] = [];
  gearboxOptions: string[] = [];
  oilStatus = ' ';
  oilStatusCss: string;
  fuelStatus = ' '
  fuelStatusCss: string;
  btnStatus1 = true;
  btnStatus2 = true;
  btnStatusFinal = true;

  allPersistEquipment : Equipment[] = [];
  dataSource: any;

  constructor(public equipmentService: EquipmentService,
              public notification: NotificationFlashService) { }

  ngOnInit(): void {
    this.initializeFormGroupe();
    this.getOptionEquipments();
    this. getAllPersistEquipment();
  }

  onSubmit() {
    if (this.equipmentService.form.valid) {
      this.equipment.fuelSystem = this.equipmentService.form.get('fuelSystem').value;
      this.equipment.gearboxOptions = this.equipmentService.form.get('gearboxOptions').value;
      this.equipment.oilSystem = this.equipmentService.form.get('oilSystems').value;

      this.equipmentService.addEquipment(this.equipment).subscribe(
        data => {
          this.notification.success("Success save a new Option Equipment");
          this.onClean();
          this.getAllPersistEquipment();
        },
        err => {
          this.notification.errorFlash(err.err);
        }
      )
    }
    this.btnStatusFinal = true;
  }

  onClean() {
    this.initializeFormGroupe();
    this.btnStatusFinal = true;
    this.oilStatus = '';
    this.fuelStatus = ''
  }

  getOptionEquipments(){
    this.equipmentService.getOptionEquipments().subscribe(
      data => {
        this.optionEquipments = data;
        for ( var i=0; i<data.length;i++) {
          if (data[i].name === 'Oil Systems') {
            for (var j = 0; j < data[i].equipmentConfigs.length; j++) {
              this.oilSystems.push(data[i].equipmentConfigs[j].value);
            }
          }
            if (data[i].name === 'Fuel System') {
              for (var j = 0; j < data[i].equipmentConfigs.length; j++) {
                this.fuelSystems.push(data[i].equipmentConfigs[j].value);
              }
            }
              if (data[i].name === 'Gearbox Options') {
                for (var j = 0; j < data[i].equipmentConfigs.length; j++) {
                  this.gearboxOptions.push(data[i].equipmentConfigs[j].value);
                }
          }
        }
      },
      err => {
        this.notification.errorFlash(err.err);
      }
    )
  }

  initializeFormGroupe() {
    this.equipmentService.form.setValue({
      id:'',
      oilSystems: this.oilSystems,
      fuelSystem: this.fuelSystems,
      gearboxOptions: this.gearboxOptions
    });
  }

  checkOilValue(event){
    this.oilStatusCss = "text-dark bg-warning";
    this.oilStatus = "In Progress...";
    this.oilValue.value = event.value;
    this.equipmentService.checkOilValue(this.oilValue).subscribe(
      data => {
        if (data.valid){
          this.oilStatusCss = "text-white bg-success";
          this.oilStatus = "Success !!!";
          this.btnStatus1 = false;
          if (!this.btnStatus1 && !this.btnStatus2){
            this.btnStatusFinal = false;
          }
        } else {
          this.oilStatusCss = "text-white bg-danger";
          this.oilStatus = "Error -:)";
          this.btnStatus1 = true;
          this.btnStatusFinal = true;
        }
      },
      err => {
        this.oilStatusCss = "text-white bg-danger";
        this.oilStatus = err.error;
        this.notification.errorFlash(err.error);
      }
    )
  }

  checkFuelValue(event){
    this.fuelStatusCss = "text-dark bg-warning";
    this.fuelStatus = "In Progress...";
    this.fuelValue.value = event.value;
    this.equipmentService.checkFuelValue(this.fuelValue).subscribe(
      data => {
        if (data.valid){
          this.fuelStatusCss = "text-white bg-success";
          this.fuelStatus = "Success !!!";
          this.btnStatus2 = false;
          if (!this.btnStatus1 && !this.btnStatus2){
            this.btnStatusFinal = false;
          }
        } else {
          this.fuelStatusCss = "text-white bg-danger";
          this.fuelStatus = "Error -:)";
          this.btnStatus1 = true;
          this.btnStatusFinal = true;
        }
        },
      err => {
        this.fuelStatusCss = "text-white bg-danger";
        this.fuelStatus = err.error;
        this.notification.errorFlash(err.error);
      }
    )
  }

  getAllPersistEquipment(){
    this.equipmentService.getAllPersistEquipment().subscribe(
      data => {
        this.allPersistEquipment = data;
        this.dataSource = this.allPersistEquipment;
      },
      err => {

      }
    )
  }
}
